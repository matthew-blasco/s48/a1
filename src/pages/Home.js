import {Fragment} from 'react'
import Banner from './../components/Banner'
import Course from './../components/Course'
// import CourseCard from './../components/CourseCard'

export default function Home(){
	return(
		// render navbar, banner & footer in the webpage via home.js
		<Fragment>
			<Banner/>
			<Course/>
		</Fragment>
	)
}
