import {Card, Button} from 'react-bootstrap';
import {useState} from 'react';

export default function Course({countProp}){
	
	//destructure array
	let [count, setCount] = useState(0);
	let [seats, setSeats] = useState(30);
	let [students, setStudents] = useState(0);

	const {name, description, price} = countProp;


	const clicky = () => {
		setCount(count + 1)
		if (seats === 0){
			alert('No more seats');

		}else {
			setSeats(seats - 1);
			setStudents(students + 1);
		}
	}

	return(
		<Card className="mx-2 my-5">
		  
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description</Card.Subtitle>
		    <Card.Text>
			     {description}
		    </Card.Text>
		    <Card.Subtitle>
		    	Price:
		   	</Card.Subtitle>
		   	<Card.Text>
		    	  {price}
		    </Card.Text>
		    <Card.Subtitle>Enrollees</Card.Subtitle>
		    <Card.Text>{students}</Card.Text>
		    <Card.Text>Count: {count}</Card.Text>
		    <Button className=" btn-info" onClick={clicky}>Enroll</Button>
		  </Card.Body>
		</Card>
	)
}
